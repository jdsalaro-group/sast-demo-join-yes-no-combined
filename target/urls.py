from django.urls import path
from . import views

from myapp.views import TestSQLInjection30

urlpatterns = [
    path('test-cases/', views.index, name='test_cases'),
    path('test_sql_injection/1', views.test_sql_injection1, name='test_sql_injection1'),
    path('test_sql_injection/2', views.test_sql_injection2, name='test_sql_injection2'),
    path('test_sql_injection/3', views.test_sql_injection3, name='test_sql_injection3'),
    path('test_sql_injection/4', views.test_sql_injection4, name='test_sql_injection4'),
    path('test_sql_injection/5', views.test_sql_injection5, name='test_sql_injection5'),
    path('test_sql_injection/6', views.test_sql_injection6, name='test_sql_injection6'),
    path('test_sql_injection/7', views.test_sql_injection7, name='test_sql_injection7'),
    path('test_sql_injection/8', views.test_sql_injection8, name='test_sql_injection8'),
    path('test_sql_injection/9', views.test_sql_injection9, name='test_sql_injection9'),
    path('test_sql_injection/10', views.test_sql_injection10, name='test_sql_injection10'),
    path('test_sql_injection/11', views.test_sql_injection11, name='test_sql_injection11'),
    path('test_sql_injection/12', views.test_sql_injection12, name='test_sql_injection12'),
    path('test_sql_injection/13', views.test_sql_injection13, name='test_sql_injection13'),
    path('test_sql_injection/14', views.test_sql_injection14, name='test_sql_injection14'),
    path('test_sql_injection/15', views.test_sql_injection15, name='test_sql_injection15'),
    path('test_sql_injection/16', views.test_sql_injection16, name='test_sql_injection16'),
    path('test_sql_injection/17', views.test_sql_injection17, name='test_sql_injection17'),
    path('test_sql_injection/18', views.test_sql_injection18, name='test_sql_injection18'),
    path('test_sql_injection/19', views.test_sql_injection19, name='test_sql_injection19'),
    path('test_sql_injection/20', views.test_sql_injection20, name='test_sql_injection20'),
    path('test_sql_injection/21', views.test_sql_injection21, name='test_sql_injection21'),
    path('test_sql_injection/22', views.test_sql_injection22, name='test_sql_injection22'),
    path('test_sql_injection/23', views.test_sql_injection23, name='test_sql_injection23'),
    path('test_sql_injection/24', views.test_sql_injection24, name='test_sql_injection24'),
    path('test_sql_injection/25', views.test_sql_injection25, name='test_sql_injection25'),
    path('test_sql_injection/26', views.test_sql_injection26, name='test_sql_injection26'),
    path('test_sql_injection/27', views.test_sql_injection27, name='test_sql_injection27'),
    path('test_sql_injection/28/<uname>/', views.test_sql_injection28, name='test_sql_injection28'),


    # path("about/", TemplateView.as_view(template_name="index.html")),
    path('test_sql_injection30/', TestSQLInjection30.as_view()),


    path('test_sql_injection/29', views.test_sql_injection29, name='test_sql_injection29'),
    # path('test_sql_injection/30', views.test_sql_injection30, name='test_sql_injection30'),
    # path('test_sql_injection/31', views.test_sql_injection31, name='test_sql_injection31'),
]
